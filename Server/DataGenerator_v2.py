import numpy as np
import matplotlib.pyplot as plt
import random
from Owner_input_variables import Owner_input, breed_input, neutered_input

class Kcal_info:
    kcal = []
    New_Kcal_val = 0


def Data(dim):
    global neut
    neut = neutered_input()
    if  neut == True:
        min_value = 0.4
        max_value = 0.8
    else:
        min_value = 0.6
        max_value = 1.0

    Owner_input.breed = breed_input()
    if Owner_input.breed == 1:
        val = 0.8
    elif Owner_input.breed == 2:
        val = 3
    else:
        val = 1
        
    global kcal, x_axis
    # Using uniform() method
    #rnd_val = list(np.random.uniform(low=min_value, high=max_value, size=(dim,)))
    #print(rnd_val)
    Kcal_info.kcal = (np.random.uniform(low=min_value, high=max_value, size=(dim,))*1000*val)
    x_axis = np.arange(0, dim, 1)
    #plt.scatter(x_axis,kcal,color ='lavender')
    #plt.show()
    return Kcal_info.kcal, x_axis


def Collar_daily():
    if  neut == True:
        min_value = 0.4
        max_value = 0.8
    else:
        min_value = 0.6
        max_value = 1.0

    if Owner_input.breed == 1:
        val = 0.8
    elif Owner_input.breed == 2:
        val = 3
    else:
        val = 1

    Kcal_info.New_Kcal_val = (np.random.uniform(low=min_value, high=max_value, size=1)*1000*val)
    return Kcal_info.New_Kcal_val

# Data(360)
# print(Kcal_info.kcal)

# Collar_daily()
# print(Kcal_info.New_Kcal_val)
