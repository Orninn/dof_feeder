from types import new_class
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

from Owner_input_variables import*
from RER_Calculator import RER_Calc
from DataGenerator_v2 import Kcal_info, Data, Collar_daily

import paho.mqtt.client as mqtt
import time


#########################################################################################################################
# FUNCTIONS

# Linear regression algorithm
def linear_regression(x_var,y_var,dim):
    global r2
    global coef
    global interc
    h_dim = int(dim/2)
    cal_y = y_var[:, np.newaxis]
    cal_y_train = cal_y[:h_dim]  # splitting data into training and test sets
    cal_y_test = cal_y[h_dim:]
    cal_x = x_var.reshape(-1, 1)
    cal_x_train = cal_x[:h_dim]  # splitting targets into training and test sets
    cal_x_test = cal_x[h_dim:]
    regr = linear_model.LinearRegression()  # Linear regression object
    regr.fit(cal_x_train, cal_y_train)  # Use training sets to train the model

    cal_y_pred = regr.predict(cal_x_test)  # Make predictions
    coef = regr.coef_
    interc = regr.intercept_
    mean_squared_error(cal_y_test, cal_y_pred)

    r2 = r2_score(cal_y_test, cal_y_pred)  # Variance score

    # plt.scatter(cal_x_test, cal_y_test, color='lavender')
    # plt.plot(cal_x_test, cal_y_pred, color='pink', linewidth=3)
    # plt.xticks(())
    # plt.yticks(())
    # plt.show()
    #print(coef)
    return coef, interc

# IoT Operations
def IoT_Calculations(intervall):
    while True:
        Kcal_info.New_Kcal_val = Collar_daily()
        Kcal_info.kcal = np.append(Kcal_info.kcal, Kcal_info.New_Kcal_val)
        new_dim = len(Kcal_info.kcal)
        print(new_dim)
        x_axis = np.arange(0, new_dim, 1)
        coef, interc = linear_regression(x_axis, Kcal_info.kcal, new_dim)

        # Prediction of total amount of calories burned is given by collar values + RER
        collar_value = int(coef.item()*(len(x_axis)+1) + interc.item())
        # print(collar_value)
        Tot_Kcal = int(collar_value + RER)
        print('Predicted energy consumption: %d Kcal' %(Tot_Kcal))

        # Weight of food calculation
        Total_food_weight = int(Tot_Kcal/Owner_input.Kcal_grams)
        #print('The total weight of the food is: %d grams' %(Total_food_weight))

        food_portion = int(Total_food_weight/number_of_portions)
        print('Portion weights %d grams' %food_portion)

        client.publish("food/daily_dose", food_portion)

        time.sleep(intervall)
      
#####################################################################################################
# START

# Ask owner for inputs
Owner_input.age = age_input()
#Owner_input.breed = breed_input()
Owner_input.Kcal_grams = Kcal_grams_input()

# Calculate RER based on dog weight
RER = RER_Calc()

# Portion calculationon
if Owner_input.age <= 0.8:
    number_of_portions = 3
else:
    number_of_portions = 2
print('The dog will eat %d times a day' %number_of_portions)

# Hardcode dimension of previous data 
dim = 20

# Get data from collar
[Kcal_info.kcal, x_axis] = Data(dim)
coef, interc = linear_regression(x_axis, Kcal_info.kcal, dim)

# Prediction of total amount of calories burned is given by collar values + RER
collar_value = int(coef.item()*(len(x_axis)+1) + interc.item())
# print(collar_value)

Tot_Kcal = int(collar_value + RER)
print('The predicted energy consumption for the next day is: %d Kcal' %(Tot_Kcal))

# Weight of food calculation
Total_food_weight = int(Tot_Kcal/Owner_input.Kcal_grams)
print('The total weight of the food is: %d grams' %(Total_food_weight))

food_portion = int(Total_food_weight/number_of_portions)
print('The portion of food weights %d grams' %food_portion)

################################################################################################################################
# MQTT communication

def on_connect(client, userdata, flags,rc):
    print("connected with result code: "+str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("localhost", 1883,60)

client.publish("dev/test", "msg recived")

client.publish("food/daily_dose", food_portion)
client.publish("food/number_of_portions", number_of_portions)
time.sleep(1)

client.loop_forever()


IoT_Calculations(30)