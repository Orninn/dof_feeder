import numpy as np
import matplotlib.pyplot as plt

from DataGenerator import calories, x_axis
from RER_Calculator import RER_value

from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

from Owner_input_variables import*


# Linear regression algorithm
def linear_regression(x_var,y_var):

    global r2
    global coef
    global interc
    cal_y = y_var[:, np.newaxis]
    cal_y_train = cal_y[:200]  # splitting data into training and test sets
    cal_y_test = cal_y[200:]
    cal_x = x_var.reshape(-1, 1)
    cal_x_train = cal_x[:200]  # splitting targets into training and test sets
    cal_x_test = cal_x[200:]
    regr = linear_model.LinearRegression()  # Linear regression object
    regr.fit(cal_x_train, cal_y_train)  # Use training sets to train the model

    cal_y_pred = regr.predict(cal_x_test)  # Make predictions
    coef = regr.coef_
    interc = regr.intercept_
    mean_squared_error(cal_y_test, cal_y_pred)

    r2 = r2_score(cal_y_test, cal_y_pred)  # Variance score

    plt.scatter(cal_x_test, cal_y_test, color='lavender')
    plt.plot(cal_x_test, cal_y_pred, color='pink', linewidth=3)
    plt.xticks(())
    plt.yticks(())
    plt.show()

    return 
    #r2, coef, interc

linear_regression(x_axis, calories)
# print(r2)
# print(coef)
# print(interc)

# Prediction of total amount of calories burned is given by collar values + RER
collar_value = coef.item()*(len(x_axis)+1) + interc.item()
# print(collar_value)

Tot_Kcal = collar_value + RER_value
# print('The dog predicted consumption of the dog for the next day is: %d .join(Tot_Kcal)')
print('The predicted energy consumption for the next day is: %d Kcal' %(Tot_Kcal))

Kcal_grams = 3.5
# Weight of food calculation
Total_food_weight = Tot_Kcal/Kcal_grams
# print(Total_food_weight)

