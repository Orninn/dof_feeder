import numpy as np
import matplotlib.pyplot as plt

from Owner_input_variables import neutered_input

def Data(dim):
    neut = neutered_input()
    if  neut == True:
        min_value = 0.4
        max_value = 0.8
    else:
        min_value = 0.6
        max_value = 1.0

    global kcal, x_axis
    # Using uniform() method
    rnd_val = np.random.uniform(low=min_value, high=max_value, size=(dim,))
    x_axis = np.arange(0, dim, 1)  
    plt.scatter(x_axis,rnd_val,color ='lavender')
    plt.show()
    kcal = rnd_val * 1000
    return kcal, x_axis
    