# This file is used to get the needed variables values from the owner

class Owner_input:
    age = 0
    breed = None
    neutered = None
    weight = 0
    Kcal_grams = 0

def age_input():
    while True:
        try:
            Owner_input.age = float(input('Enter the dog age (in years and months, ex: 2.5 - two years and five months): '))
        except ValueError:
            print("Sorry, answer again with a number.")
            age_input()
        break
    return Owner_input.age

def breed_input():
    print('Enter one of the following options:')
    print('A = Inactive/obese prone dog')
    print('B = Active/working dog')
    print('C = Normal dog')
    input_breed = input('Enter selected letter: ')
    if input_breed == 'A':
        breed = 1
        return breed
    if input_breed == 'B':
        breed = 2
        return breed
    if input_breed == 'C':
        breed = 3
        return breed
    else:
        print('Enter one of the letters')
        breed_input()
    return
    

def neutered_input():
    Owner_input.neutered = None
    input_value = input('Is the dog neutered? (Yes/No) ')
    if input_value == 'Yes':
        Owner_input.neutered = True
        return Owner_input.neutered
    if input_value == 'No':
        Owner_input.neutered = False
        return Owner_input.neutered
    else:
        print("Please enter an answer again, it must be Yes or No")
        neutered_input()
    return
        
def dog_weight_input():
    while True:
        try:
            Owner_input.weight = int(input('Enter the dog weight (in kg): '))
        except ValueError:
            print("Sorry, answer again with a number.")
            dog_weight_input()
        break
    return Owner_input.weight

def Kcal_grams_input():
    while True:
        try:
            Owner_input.Kcal_grams = int(input('Enter the value of Kcal per gram of the food: '))
        except ValueError:
            print("Sorry, answer again with a number.")
            Kcal_grams_input()
        break
    return Owner_input.Kcal_grams


