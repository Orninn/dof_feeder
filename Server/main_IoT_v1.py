import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

from Owner_input_variables import*
from RER_Calculator import RER_Calc
from DataGenerator_v1 import*

import paho.mqtt.client as mqtt
import time

# Linear regression algorithm
def linear_regression(x_var,y_var):
    global r2
    global coef
    global interc
    cal_y = y_var[:, np.newaxis]
    cal_y_train = cal_y[:200]  # splitting data into training and test sets
    cal_y_test = cal_y[200:]
    cal_x = x_var.reshape(-1, 1)
    cal_x_train = cal_x[:200]  # splitting targets into training and test sets
    cal_x_test = cal_x[200:]
    regr = linear_model.LinearRegression()  # Linear regression object
    regr.fit(cal_x_train, cal_y_train)  # Use training sets to train the model

    cal_y_pred = regr.predict(cal_x_test)  # Make predictions
    coef = regr.coef_
    interc = regr.intercept_
    mean_squared_error(cal_y_test, cal_y_pred)

    r2 = r2_score(cal_y_test, cal_y_pred)  # Variance score

    plt.scatter(cal_x_test, cal_y_test, color='lavender')
    plt.plot(cal_x_test, cal_y_pred, color='pink', linewidth=3)
    plt.xticks(())
    plt.yticks(())
    plt.show()

    return 

# Ask owner for inputs
age = age_input()
breed = breed_input()
Kcal_grams = Kcal_grams_input()
# Calculate RER based on dog weight
RER = RER_Calc()
# print(RER)

# Get data from collar
[kcal, x_axis] = Data(365)

linear_regression(x_axis, kcal)
# print(r2)
# print(coef)
# print(interc)

# Prediction of total amount of calories burned is given by collar values + RER
collar_value = coef.item()*(len(x_axis)+1) + interc.item()
# print(collar_value)

Tot_Kcal = collar_value + RER
# print('The dog predicted consumption of the dog for the next day is: %d .join(Tot_Kcal)')
print('The predicted energy consumption for the next day is: %d Kcal' %(Tot_Kcal))


# Weight of food calculation
Total_food_weight = Tot_Kcal/Kcal_grams
print('The total weight of the food is: %f grams' %(Total_food_weight))


# Portion calculationon
if age <= 0.8:
    number_of_portions = 3
else:
    number_of_portions = 2
print('The dog will eat %d times a day' %number_of_portions)
food_portion = Total_food_weight/number_of_portions
print('The portion of food weights %f grams' %food_portion)

################################################################################################################################
# MQTT communication

def on_connect(client, userdata, flags,rc):
    print("connected with result code: "+str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("localhost", 1883,60)

client.publish("dev/test", "msg recived")

i = 0
while i < 10000:
    client.publish("food/daily_dose", food_portion)
    time.sleep(1)
    client.publish("food/daily_dose", food_portion)
    i = i+1
    print(i)
    time.sleep(1)

client.loop_forever()

