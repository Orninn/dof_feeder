# This file is used to get the needed variables values from the owner

def age_input():
    global age
    while True:
        try:
            age = float(input('Enter the dog age (in years and months, ex: 2.5 - two years and five months): '))
        except ValueError:
            print("Sorry, answer again with a number.")
            age_input()
        break
    return age

def breed_input():
    global breed
    breed = input('Enter the dog breed: ')
    return breed

def neutered_input():
    global neutered
    neutered = None
    input_value = input('Is the dog neutered? (Yes/No) ')
    if input_value == 'Yes':
        neutered = True
        return neutered
    if input_value == 'No':
        neutered = False
        return neutered
    else:
        print("Please enter an answer again, it must be Yes or No")
        neutered_input()
    return
        
def dog_weight_input():
    while True:
        try:
            weight = int(input('Enter the dog weight (in kg): '))
        except ValueError:
            print("Sorry, answer again with a number.")
            dog_weight_input()
        break
    return weight

def Kcal_grams_input():
    global Kcal_grams
    while True:
        try:
            Kcal_grams = int(input('Enter the value of Kcal per gram of the food: '))
        except ValueError:
            print("Sorry, answer again with a number.")
            Kcal_grams_input()
        break
    return Kcal_grams





 