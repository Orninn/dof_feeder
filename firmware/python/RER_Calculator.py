# RER(Resting Energy Requirement)
# It is used to calculate the amount of calories that are burned for essential body functions

# Input variables from the owner
from Owner_input_variables import dog_weight_input

def RER_Calc():
    weight = dog_weight_input()
    RER = 70*pow(weight, 0.75)
    return RER
# In Kcal

