#include <Arduino.h>
#include <Ethernet.h>
#include <MQTT.h>
#include <DFRobot_HX711_I2C.h>

#define BUTTON 5
#define MOTOR 2
#define DELIVER_LED 6

int get_bowl_weight();
void dispense_food(int weight);
bool time_to_feed();
void deliver_food();
void read_scale();
void manage_time();

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 0, 140};  // <- change to match your network

//DFRobot_HX711_I2C MyScale(&Wire,/*addr=*/0x64);
DFRobot_HX711_I2C MyScale;

EthernetClient net;
MQTTClient client;

unsigned long lastMillis = 0;
String topic_food_dose = "food/daily_dose";
String topic_feed_schedule = "food/number_of_portions";
int food_dose = 500;
int number_of_portions = 3;
float start_time = millis();
float day_lenght = 40000;
float feed_time = day_lenght/2/number_of_portions*0.8;
bool is_night_time = false;
int feed_count = 0;

void connect() {
  Serial.print("connecting...");
  while (!client.connect("arduino", "public", "public")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  client.subscribe("#");
}

void messageReceived(String &topic, String &payload) {
  
  int length = sizeof(payload);
  if(!topic.compareTo(topic_food_dose)){
    char buf [length+1];
    for(int i = 0; i<length; i++){
      buf[i]=payload[i];
    }
    
    food_dose = atoi(buf);
    Serial.print("New dose recived: ");
    Serial.println(food_dose);
  }
  if(!topic.compareTo(topic_feed_schedule)){
    char buf [length+1];
    for(int i = 0; i<length; i++){
      buf[i]=payload[i];
    }
    number_of_portions = atoi(buf);
    feed_time = day_lenght/2/number_of_portions;
    Serial.print("New feefing scedual recived: ");
    Serial.println(number_of_portions);
  }
}

void setup() {
  Serial.begin(115200);
  Ethernet.begin(mac, ip);
  
  while (!MyScale.begin()) {
    Serial.println("The initialization of the chip is failed, please confirm whether the chip connection is correct");
    delay(1000);
  }
  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported
  // by Arduino. You need to set the IP address directly.
  client.begin("192.168.0.103", net);
  client.onMessage(messageReceived);

  connect();
  Serial.println(feed_time);
}

void loop() {
  client.loop();
  
  if(time_to_feed()){
    dispense_food(food_dose);
    deliver_food();
  }
  manage_time();
}

bool time_to_feed(){

  if(is_night_time == true){
    return false;
  }

  if(MyScale.readWeight() >= food_dose*0.8){
    Serial.println("Bowl already fool delivery canceled");
    return false;
  }

  if(millis()-start_time > feed_time && feed_count<1){
    Serial.println("Time to feed!");
    feed_count++;
    Serial.print("Feed number:");
    Serial.println(feed_count);
    return true;
  }
  if(millis()-start_time > feed_time*2 && feed_count<2){
    Serial.println("Time to feed!");
    feed_count++;
    Serial.print("Feed number:");
    Serial.println(feed_count);
    return true;
  }
  if(millis()-start_time > feed_time*3 && feed_count<3){
    Serial.println("Time to feed!");
    feed_count++;
    Serial.print("Feed number:");
    Serial.println(feed_count);
    return true;
  }
  return false;
}

void dispense_food(int weight){
  while(MyScale.readWeight() < weight){
    //Spin propeller motor
    digitalWrite(MOTOR,HIGH);
    Serial.println(MyScale.readWeight());
  }
  // Stop propeller motor
  digitalWrite(MOTOR, LOW);
}

void deliver_food(){
  digitalWrite(DELIVER_LED,HIGH);
  delay(2000);
  digitalWrite(DELIVER_LED,LOW);
}

void manage_time(){
  if(millis()-start_time > day_lenght){
    start_time = millis();
    feed_count = 0;
    Serial.println("A new day starts");
  }

  if(millis()-start_time < day_lenght/2){
    is_night_time = false;
    Serial.println("Day time");
  }else{
    is_night_time = true;
    Serial.println("Night time");
  }
}